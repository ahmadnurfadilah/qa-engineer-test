let numbers = [-6,4,-5,8,-10,0,8];
let n = numbers.length;
console.log("the maximum product sub-array is having product " + maxSubarrayProduct(numbers, n));

function maxSubarrayProduct(arr, n) {
  let result = arr[0];
 
  for (let i = 0; i < n; i++) {
    let mul = arr[i];
    for (let j = i + 1; j < n; j++) {
        result = Math.max(result, mul);
        mul *= arr[j];
    }
    result = Math.max(result, mul);
  }
  return result;
}