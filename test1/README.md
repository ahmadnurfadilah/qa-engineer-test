# QA Engineer Test 1

Thi directory (test1) is the result of **QA Engineer Test 1**

## Prerequisite
1. You have installed NodeJS
2. Basic knowledge of terminal

## How To
1. Open your terminal
2. Clone this repository to your local machine
    ```bash
    git clone https://gitlab.com/ahmadnurfadilah/qa-engineer-test
    ```
3. Open **qa-engineer-test/test1** directory
    ```bash
    cd qa-engineer-test/test1
    ```
4. Run each file with NodeJS ({1-4}.js)
    ```bash
    node 1.js
    node 2.js
    node 3.js
    node 4.js
    ```
## Questions?

Contact me at:
**hi@ahmadnf.xyz**
